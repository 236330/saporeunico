from decimal import Decimal

from django.conf import settings
from checkout.models import DeliveryOptions
from store.models import Product


class Basket():
    """
    Una classe basket base, dove vengono inseriti comportamenti default
    che possono essere ereditati o overrided, a seconda della necessità
    """
    def __init__(self, request):
        self.session = request.session
        basket = self.session.get('skey')  #nome session key
        if'skey' not in request.session:
            basket = self.session['skey'] = {}  #impostiamo il carrello-basket a vuoto all'inizio
        self.basket = basket  #se la sessione è già avviata, il carrello non è vuoto

    def add(self, product, qty):
        """
        Aggiungo e aggiorno il carrello dell'utente della sessione

        """
        product_id = product.id

        if product_id in self.basket:
            self.basket[product_id]['qty'] = qty
        else:
            self.basket[product_id] = {'price': str(product.price), 'qty': qty}

        self.save()

    def __iter__(self):
        """
        raccoglie i prodotti_id dai dati della sessione per interrogare
        il database e restituire i prodotti
        :return: iterabile
        """

        product_ids = self.basket.keys()
        products = Product.products.filter(id__in=product_ids)
        basket = self.basket.copy()

        for product in products:
            basket[str(product.id)]['product'] = product

        for item in basket.values():
            item['price'] = Decimal(item['price'])
            item['total_price'] = item['price'] * item['qty']
            yield item

    def __len__(self):
        """
        Ottiene le informazioni-data dal carrello e conta le quantità di oggetti in esso
        :return: somma delle quantità di oggetti nel carrello
        """
        return sum(item['qty'] for item in self.basket.values())

    def get_total_elements(self):
        return self.__len__()

    def get_total_price(self):
        return sum(Decimal(item['price']) * item['qty'] for item in self.basket.values())

    def get_delivery_price(self):
        newprice = 0.00

        if "purchase" in self.session:
            newprice = DeliveryOptions.objects.get(id=self.session["purchase"]["delivery_id"]).delivery_price

        return newprice

    def basket_update_delivery(self, deliveryprice=0):
        total = Decimal(deliveryprice)
        return total

    def delete(self, product):
        """
        delete item from session data

        """
        product_id = str(product)

        if product_id in self.basket:
            del self.basket[product_id]

        self.save()

    def update(self, product, qty):
        """
        Update values in session data
        """
        product_id = str(product)
        qty = qty

        if product_id in self.basket:
            self.basket[product_id]['qty'] = qty

        self.save()

    def clear(self):
        # Remove basket from session
        del self.session['skey']
        del self.session["address"]
        del self.session["purchase"]
        self.save()

    def save(self):
        self.session.modified = True

