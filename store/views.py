from django.shortcuts import get_object_or_404, render

from .models import Category, Product


def search(request):
    if request.method == 'POST':
        searched = request.POST['searched']# quello che inserito nella barra di ricerca
        products = Product.object.filter(name__contains=searched)
        return render(request, 'store/search_products.html', {'searched': searched, 'products': products})
    else:
        return render(request, 'store/search_products.html', {})


def product_all(request):
    products = Product.products.all().order_by('name')
    return render(request, 'store/index.html', {'products': products})


def product_detail(request, slug):
    product = get_object_or_404(Product, slug=slug, in_stock=True)
    return render(request, './store/single.html', {'product': product})


def category_list(request, category_slug):
    category = get_object_or_404(Category, slug=category_slug)
    products = Product.object.filter(category=category).order_by('name')
    return render(request, 'store/category.html', {'category': category, 'products': products})



