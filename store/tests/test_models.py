from django.test import TestCase

from store.models import Category, Product


class TestCategoriesModel(TestCase):

    def setUp(self):
        self.data1 = Category.objects.create(name='frutta', slug='frutta')

    def test_category_model_entry(self):
        """
        Test category model data insertion/types/field attributes
        """
        data = self.data1
        self.assertTrue(isinstance(data, Category))
        self.assertEqual(str(data), 'frutta')


class TestProductsModel(TestCase):
    def setUp(self):
        Category.objects.create(name='frutta', slug='frutta')
        self.data1 = Product.products.create(category_id=1, name='fragole',
                                             slug='fragole', price='3.50',
                                             image='default')
        self.data2 = Product.products.create(category_id=1, name='ciliegie',
                                             slug='ciliegie', price='20.00',
                                             image='default', is_active=False)

    def test_products_model_entry(self):
        """
        Test product model data insertion/types/field attributes
        """
        data = self.data1
        self.assertTrue(isinstance(data, Product))
        self.assertEqual(str(data), 'fragole')
