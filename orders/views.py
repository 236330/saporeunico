
from django.shortcuts import render
from django.http.response import JsonResponse
from django.contrib.auth.decorators import login_required


from basket.basket import Basket
from .models import Order, OrderItem


@login_required
def BasketView(request):
    basket = Basket(request)
    return render(request, 'orders/home.html')


def add(request):
    basket = Basket(request)
    if request.POST.get('action') == 'post':

        user_id = request.user.id
        order_key = request.POST.get('order_key')
        # baskettotal = basket.get_total_price() è commetato perchè non ha senso con i prodotti che ho e il prezzo non
        # sarebbe corretto

        # check if order exists
        if Order.objects.filter(order_key=order_key).exists():
            pass
        else:
            order = Order.objects.create(user_id=user_id, full_name='name', address1='add1', address2='add2',
                                         order_key=order_key)
            order_id = order.pk
            for item in basket:
                OrderItem.objects.create(order_id=order_id, product=item['product'], price=item['price'],
                                         quantity=item['qty'])
        response = JsonResponse({'success': 'Return something'})
        return response


def user_orders(request):
    user_id = request.user.id
    orders = Order.objects.filter(user_id=user_id)
    return orders


def order_confirmation(data):
    Order.objects.filter(order_key=data).update(order_status=True)
