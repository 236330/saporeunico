from django.urls import path

from . import views

app_name = 'orders'

urlpatterns = [
    path('', views.BasketView, name='basket'),
    path('add/', views.add, name='add'),
]