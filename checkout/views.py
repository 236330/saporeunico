from account.models import Address, Customer
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, JsonResponse
from django.contrib.auth.decorators import login_required
from basket.basket import Basket
from .models import DeliveryOptions
from django.contrib import messages
from orders.models import Order, OrderItem


@login_required
def deliverychoices(request):
    deliveryoptions = DeliveryOptions.objects.filter(is_active = True)
    # ritorno solo le modalità di spedizione che sono disponibili che posso controllare
    # con il valore booleano is_active
    return render(request, "checkout/delivery_options.html", {"deliveryoptions": deliveryoptions})


@login_required
def basket_update_delivery(request):
    basket = Basket(request)
    if request.POST.get("action") == "post":
        delivery_option = int(request.POST.get("deliveryoption"))
        delivery_type = DeliveryOptions.objects.get(id=delivery_option)
        updated_total_price = basket.basket_update_delivery(delivery_type.delivery_price)

        session = request.session
        if "purchase" not in request.session:
            session["purchase"] = {
                "delivery_id": delivery_type.id,
            }
        else:
            session["purchase"]["delivery_id"] = delivery_type.id
            session.modified = True

        response = JsonResponse({"total": updated_total_price, "delivery_price": delivery_type.delivery_price})
        return response


@login_required
def delivery_address(request):
    session = request.session
    # se non è selezionata nessuna modalità di consegna, rispedisco l'utente alla pagina di selezione
    if "purchase" not in request.session:
        messages.success(request, "Devi prima selezionare un metodo di consegna!")
        return HttpResponseRedirect(request.META["HTTP_REFERER"])

    addresses = Address.objects.filter(customer=request.user).order_by("-default")

    if "address" not in request.session:
        session["address"] = {"address_id": str(addresses[0].id)}
    else:
        session["address"]["address_id"] = str(addresses[0].id)
        session.modified = True

    return render(request, "checkout/delivery_address.html", {"addresses": addresses})


@login_required()
def order_complete(request):
    user_id = request.user.id
    basket = Basket(request)

    c = get_object_or_404(Customer)
    indirizzo = get_object_or_404(Address)
    orderid = Order.pk
    order = Order.objects.create(
        user_id=user_id,
        full_name=c.name,
        phone=c.mobile,
        address1=indirizzo.address_line,
        address2=indirizzo.address_line2,
        city=indirizzo.town_city,
        order_key=orderid,
        order_status=True,

    )
    order_id = order.pk
    for item in basket:
        OrderItem.objects.create(
            order_id=order_id,
            product=item["product"], price=item["price"],
            quantity=item["qty"])

    basket.clear()
    basket.save()
    return render(request, "checkout/order_successful.html", {})



@login_required()
def order_success(request):
    basket = Basket(request)
    basket.clear()
    return render(request, "checkout/order_successful.html", {})
